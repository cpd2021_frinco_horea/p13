public class Runner {
    public Runner() {
    }

    public static void main(String[] args) {
        Imprimanta imprimanta = new Imprimanta();

        Functionar f1 = new Functionar(imprimanta, 1000);
        Functionar f2 = new Functionar(imprimanta, 2000);
        Functionar f3 = new Functionar(imprimanta, 3000);
        Functionar f4 = new Functionar(imprimanta, 4000);
        Functionar f5 = new Functionar(imprimanta, 5000);
        Functionar f6 = new Functionar(imprimanta, 3000);
        Functionar f7 = new Functionar(imprimanta, 2500);
        Functionar f8 = new Functionar(imprimanta, 1000);

        f1.start();
        f2.start();
        f3.start();
        f4.start();
        f5.start();
        f6.start();
        f7.start();
        f8.start();
    }
}