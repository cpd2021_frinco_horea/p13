public class Imprimanta {

    public synchronized void print(String s) {
        try {
            System.out.println("Functionar " + Thread.currentThread().getId() + ": started printing");
            Thread.sleep(1000);
            System.out.println("Functionar " + Thread.currentThread().getId() + ": " + s);
            System.out.println("Functionar " + Thread.currentThread().getId() + ": finished printing");
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
