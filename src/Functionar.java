public class Functionar extends Thread {

    private Imprimanta imprimanta;
    private Integer ritm;

    public Functionar(Imprimanta imprimanta, Integer ritm) {
        this.imprimanta = imprimanta;
        this.ritm = ritm;
    }


    public void run() {
        for(int i = 0; i < 10; ++i) {
            imprimanta.print("mesaj generic");
            try {
                Thread.sleep(ritm);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
